from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class Stroy_test(TestCase):
    def test_lab_3_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landpage.html')

    def test_lab_9_url_is_exist(self):
        response = Client().get('/personal/')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/personal/')
        self.assertTemplateUsed(response, 'personal.html')

    def test_lab_10_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_lab_6_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_profile_template(self):
        response = Client().get('/profile/')
        print(response)
        self.assertTemplateUsed(response, 'profile.html')

    def test_lab_10_url_is_exist(self):
        response = Client().get('/profile2/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_profile_template(self):
        response = Client().get('/profile2/')
        print(response)
        self.assertTemplateUsed(response, 'profile2.html')

    def test_lab_9_url_is_exist(self):
        response = Client().get('/booklist/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_profile_template(self):
        response = Client().get('/booklist/')
        self.assertTemplateUsed(response, 'book.html')

    def test_delete(self):
        response = Client().get('/delete/')
        self.assertEqual(response.status_code, 301)

    def test_login(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 301)

    def test_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 301)

    def test_subscribe_should_return_status_subscribe_true(self):
        response = Client().post('/subscribe/', {
            "email": "muh@gmail.com",
            "nama": "muh",
            "password":  "password",
        })
        self.assertEqual(response.json()['status_subscribe'], True)




    # def test_lab_9_url_is_exist(self):
    #     response = Client().get('/checkEmail/')
    #     self.assertEqual(response.status_code,301)

    

    # def test_lab6_using_index_func(self):
    #     found = resolve('/profile/')
    #     self.assertEqual(found.func, profile)
    

    # def test_lab6_using_index_func(self):
    #     found = resolve('/subscribe/')
    #     self.assertEqual(found.func, subscribe)

    # def test_lab6_using_index_func(self):
    #     found = resolve('/booklist/')
    #     self.assertEqual(found.func, booklist)


    def test_lab6_using_index_func(self):
        found = resolve('/profile2/')
        self.assertEqual(found.func, profile2)

    # def test_lab6_using_index_func(self):
    #     found = resolve('/checkEmail/')
    #     self.assertEqual(found.func, checkEmail)


    def test_lab_3_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status)

    def test_model_can_create_new_activity(self):
            #Creating a new activity
            new_activity = Status.objects.create(judul='Aku mau latihan ngoding deh', isiJudul ='mau latihan ngoding juga')

            #Retrieving all available activity
            counting_all_available_activity = Status.objects.all().count()
            self.assertEqual(counting_all_available_activity,1)

    def test_delete(self):
        Client().get('/delete')
        self.assertEqual(Status.objects.all().count(),0)

    # def test_create_anything_model(self, judul='ini judul', isiJudul='ini isi judul'):
    # 	return Status.object.create(judul= judul, isiJudul=isi Judul)
	
    
    # def test_lab6_using_index_func(self):
    #     found = resolve('/profile2/')
    #     self.assertEqual(found.func, profile2)

    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/', data={'judul': 'mau belajar', 'isiJudul' : 'abis belajar main'})
    #     counting_all_available_activity = Status.objects.all().count()
    #     self.assertEqual(counting_all_available_activity, 1)

    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(response['location'], '/')

    #     new_response = self.client.get('/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('Maen Dota Kayaknya Enak', html_response)

    def test_lab5_post_success_and_render_the_resul(self):
    	test = 'Anonymous'
    	response_post = Client().post('/', {'judul': test, 'isiJudul': test})
    	self.assertEqual(response_post.status_code, 200)

    	response = Client().get('/')
    	html_response = response.content.decode('utf8')
    	self.assertIn(test, html_response)

# class NewVisitorTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('-dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']

#         self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
#         self.browser.implicitly_wait(3)
#         super(NewVisitorTest,self).setUp()


#     def test_can_fill_itself(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         time.sleep(3) # Let the user actually see something
#         input_box = self.browser.find_element_by_id('id_judul')
#         input_box2 = self.browser.find_element_by_id('id_isiJudul')
#         input_box.send_keys('Halo nama saya jija')
#         input_box2.send_keys('saya orang ganteng')
#         input_box.submit()
#         time.sleep(3)
#         self.assertIn("jija", self.browser.page_source)

#     def test_css_for_form(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         form = self.browser.find_element_by_tag_name('form')
#         self.assertIn('text-white', form.get_attribute('class'))

#     def test_css_for_title(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         title = self.browser.find_element_by_tag_name('title')
#         self.assertIn('Landpage', title.get_attribute('innerHTML'))

#     def test_css_for_background_color_h1(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         header = self.browser.find_element_by_tag_name('h1').value_of_css_property('background-color')
#         self.assertIn(header, 'rgba(30, 144, 255, 1)')

#     def test_css_for_font_color_h1(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         header2 = self.browser.find_element_by_tag_name('h1').value_of_css_property('color')
#         self.assertIn(header2, 'rgba(255, 255, 255, 1)')

#     #test for personal html

#     def test_button_color(self):
#         self.browser.get('http://muhlab6.herokuapp.com/personal')
#         time.sleep(3) # Let the user actually see something
#         self.browser.find_element_by_id("co").click()
#         self.browser.find_element_by_id("red").click()
#         self.browser.find_element_by_id("black").click()        

#     def test_css_for_button(self):
#         self.browser.get('http://muhlab6.herokuapp.com/personal')
#         form = self.browser.find_element_by_tag_name('button')
#         self.assertIn('btn btn-secondary', form.get_attribute('class'))

#     def test_css_for_title(self):
#         self.browser.get('http://muhlab6.herokuapp.com/personal')
#         title = self.browser.find_element_by_tag_name('title')
#         self.assertIn('Persnonal', title.get_attribute('innerHTML'))

#     def test_css_for_font_color_h1(self):
#         self.browser.get('http://muhlab6.herokuapp.com/personal')
#         header3 = self.browser.find_element_by_tag_name('h1').value_of_css_property('color')
#         self.assertIn(header3, 'rgba(0, 0, 0, 1)')




# if __name__ == '__main__':
#     unittest.main(warnings='ignore')



