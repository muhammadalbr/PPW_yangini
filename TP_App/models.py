from django.db import models

class Status(models.Model):
	judul = models.CharField(max_length = 300)
	isiJudul = models.CharField(max_length = 300)
	def __str__(self):
		return "{}".format(self.judul)

class Subs(models.Model):
	nama = models.CharField(max_length = 300)
	email = models.EmailField(max_length=30, unique=True, primary_key = True)
	password = models.CharField(max_length = 300)