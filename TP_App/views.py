from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Status, Subs
from .forms import statusForm, FormSubscribe
from django.urls import reverse
from django.http import HttpResponseRedirect
import requests
import json
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

# def landpage(request):
# 	return render(request, 'landpage.html')

def profile(request):
	return render(request, 'profile.html')

def personal(request):
	return render(request, 'personal.html')

@csrf_exempt
def booklist(request):
	user = request.user
	if user is not None:
		jumlahFav = 0
		if request.method == 'POST':
			request.session['jumlahFav'] = request.POST['jumlahFav']

		if request.method != 'POST':

			if user is not None:
				jumlahFav = request.session.get('jumlahFav')
				return render(request, 'book.html', {'jumlahFav': jumlahFav})
	else :
		return HttpResponseRedirect('/login/')
	return render(request, 'book.html')

def logout(request):
	request.session.flush()
	return render(request, 'book.html')

def data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'

	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
	jsonparse = json.dumps(getJson.json())
	return HttpResponse(jsonparse)

	





def status(request):
	posted = Status.objects.all()
	if request.method == 'POST':
		form = statusForm(request.POST)
		if form.is_valid():

			form.save()
			form = statusForm()

			context = {
				'Posts' : posted,
				'statusForm' : form,
			}
			return render(request, 'landpage.html', context)

	else :
		
		form = statusForm()
		context = {
		'Posts': posted,
		'statusForm' : form,
		}
	return render(request,'landpage.html',context)

def delete(request):
	s= Status.objects.all().delete()
	print("Sdsiofljshduof")
	print(s)
	return redirect('/')

def profile2(request):
	return render(request, 'profile2.html')
	
def subscribe(request):
    form = FormSubscribe(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        status_subscribe = True
        try:
            Subs.objects.create(**data)
        except:
            status_subscribe = False
        return JsonResponse({'status_subscribe': status_subscribe})
    content = {'form': form}
    return render(request, 'subscribe.html', content)


def checkEmail(request):
    if request.method == "POST":
        email = request.POST['email']
        is_email_already_exist = Subs.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': is_email_already_exist})
