from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf import settings



from . import views
from TP_App import views as appViews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
  	url(r'^profile/$', appViews.profile),
    url(r'^delete/$', appViews.delete, name="delete"),
    url(r'^personal/$', appViews.personal),
    url(r'^booklist/$', appViews.booklist, name="home"),
    url('data',appViews.data),
    url(r'^profile2/$', appViews.profile2),
    url(r'^subscribe/$', appViews.subscribe),
    url(r'^checkEmail/$', appViews.checkEmail, name="checkEmail"),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^$', appViews.status),
    
]