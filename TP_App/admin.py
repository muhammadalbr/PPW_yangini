from django.contrib import admin
from .models import Status, Subs

admin.site.register(Status)
admin.site.register(Subs)

# Register your models here.
