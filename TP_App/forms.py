from django import forms
from TP_App.models import Status, Subs

class statusForm(forms.ModelForm):
	class Meta:
		model = Status
		fields = ['judul','isiJudul']
		widgets = {
			'judul': forms.TextInput(attrs={'class' : 'form-control'}),
			'isiJudul': forms.TextInput(attrs={'class' : 'form-control'}),
		}

		label = {
			'judul': 'Judul',
			'isiJudul': 'Isi Judul'
		}

class FormSubscribe(forms.Form):
    nama = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap', 'id' : 'nama'}))
    email = forms.CharField(required=True, max_length=30,
                            widget=forms.DateInput(attrs={'type': 'email', 'placeholder': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, max_length=10,
                               widget=forms.TextInput(attrs={'type': 'password', 'placeholder': 'Password', 'id' : 'password'}))