from django import forms
from Lab4_App.models import Schedule

class scheduleForm(forms.ModelForm):
	class Meta:
		model = Schedule
		fields = ['judul','day','time','activity','place', 'category']
		widgets = {
			'judul': forms.TextInput(attrs={'class' : 'form-control'}),
			'day': forms.TextInput(attrs={'type':'date','class' : 'form-control'}),
			'time': forms.TextInput(attrs={'type':'time', 'class': 'form-control'}),
			'activity': forms.TextInput(attrs={'class': 'form-control'}),
			'place': forms.TextInput(attrs={'class': 'form-control'}),
			'category': forms.TextInput(attrs={'class': 'form-control'}),
		}
		label = {
			'judul': 'Judul',
			'day': 'Day',
			'time': 'Time',
			'activity': 'Activity',
			'place': 'Place'
		}
