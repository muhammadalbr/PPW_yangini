from django.db import models
from django.utils import timezone
from datetime import datetime

class Schedule(models.Model):
	judul = models.CharField(max_length = 50)
	day = models.DateField();
	time = models.TimeField();
	activity = models.CharField(max_length = 50)
	place = models.CharField(max_length = 50)
	category = models.CharField(max_length = 50)
	
	def __str__(self):
		return "{}".format(self.judul)
# Create your models here.
